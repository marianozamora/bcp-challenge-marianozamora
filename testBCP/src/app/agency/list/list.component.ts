import { Component, OnInit } from '@angular/core';
import { Agency, IAgency } from '../models/agency';
import { Observable } from 'rxjs';
import { AgencyService } from '../service/agency.service';
import { Router } from '@angular/router';

@Component({
  selector: "app-list",
  templateUrl: "./list.component.html",
  styleUrls: ["./list.component.css"],
})
export class ListComponent implements OnInit {
  public agencies: Observable<IAgency[]> = null;
  constructor(private router: Router, private agencyService: AgencyService) {}

  ngOnInit() {
    this.agencies = this.agencyService.getAllAgencies();
  }

  deleteAgency(agency): void {
    const result = this.agencyService.deleteAgency(agency);
  }

  viewAgency(agency: IAgency): void {
    this.router.navigate(["agencies/view/" + agency.id]);
  }
}
