    // agencia: "Las Flores",
    // distrito: "San Juan De Lurigancho",
    // provincia: "Lima",
    // departamento: "Lima",
    // direccion: "Las Flores de Primavera 1487",
    // lat: -77.01232817,
    // lon: -12.0046896,
    //     id: 9,
    // name: "Eastern tomoto pickle 500g",
    // code: "mp500",
    // category: { name: "mango", code: "1", category: 1 },
    // unit: { name: "PCS", code: "1", category: 0 },
    // purchaseRate: 100,
    // salesRate: 110,
export interface IAgency {
  id: number;
  agencia: string;
  distrito: string;
  provincia: string;
  departamento: string;
  direccion: string;
  lat: number;
  lon: number;
}
export class Agency {
         id: number;
         agencia: string;
         distrito: string;
         provincia: string;
         departamento: string;
         direccion: string;
         lat: number;
         lon: number;
         constructor(
           agencia?: string,
           distrito?: string,
           provincia?: string,
           departamento?: string,
           direccion?: string,
           lat?: number,
           lon?: number,
         ) {
           this.agencia = agencia;
           this.distrito = distrito;
           this.provincia = provincia;
           this.departamento = departamento;
           this.direccion = direccion;
           this.lat = lat;
           this.lon = lon;
         }
       }
