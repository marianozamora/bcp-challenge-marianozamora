import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IAgency, Agency } from '../models/agency';
import data from './data';

@Injectable({
  providedIn: 'root'
})
export class AgencyService {

  private agencies:Array<Agency> =  data ;

  constructor() { }

  getAllAgencies():Observable<IAgency[]>{
    return of(this.agencies)
  }

  getAgencyById(id:number):Observable<IAgency>{
    var agency = this.agencies.find(item => item.id === id);
    return of(agency);
  }

  addNewAgency(agency:IAgency):void{
    this.agencies.sort(item => item.id)
    agency.id = this.agencies.length + 1
    this.agencies.push(agency);
  }

  deleteAgency(agency:IAgency):IAgency[]{
    const index = this.agencies.findIndex(item => item.id === agency.id);
    const deletedItem = this.agencies.splice(index,1);

    return deletedItem;
  }

  updateAgency(agency:IAgency):void{
    const index = this.agencies.findIndex(item => item.id === agency.id);
    this.agencies[index] = agency;
  }

}
