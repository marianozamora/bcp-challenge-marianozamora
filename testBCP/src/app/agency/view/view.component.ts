import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { IAgency, Agency } from '../models/agency';
import { AgencyService } from '../service/agency.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  agency$:Observable<IAgency>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private agencyService:AgencyService) { }

  ngOnInit() {

    this.agency$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
          this.agencyService.getAgencyById(Number.parseInt(params.get('id')))
        ));
    }

    editAgency(agency:IAgency):void{

      this.agency$.subscribe(agency =>{
        this.router.navigate(['agencies/edit/'+agency.id]);
      });
  }
}
