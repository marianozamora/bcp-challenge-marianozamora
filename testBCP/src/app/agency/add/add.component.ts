import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Agency, IAgency } from '../models/agency';
import { AgencyService } from '../service/agency.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit, OnDestroy {

  private observableSubscription:Array<Subscription> = [];
  formSubmitted = false;
  agencyForm = this.fb.group({});

  constructor(private fb:FormBuilder,
    private agencyService:AgencyService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit() {
    this.agencyForm.addControl('id',new FormControl(''));
    this.agencyForm.addControl('agencia',new FormControl('',[Validators.required]));
    this.agencyForm.addControl('distrito',new FormControl('',[Validators.required]));
    this.agencyForm.addControl('departamento',new FormControl('',[Validators.required]));
    this.agencyForm.addControl('provincia',new FormControl('',[Validators.required]));
    this.agencyForm.addControl('direccion',new FormControl('',[Validators.required]));
    const agency$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) =>
          this.agencyService.getAgencyById(Number.parseInt(params.get('id')))
        ));

        agency$.subscribe(agency=>{
          if(!isNullOrUndefined(agency)){
            this.agencyForm.get('id').setValue(agency.id);
            this.agencyForm.get('agencia').setValue(agency.agencia);
            this.agencyForm.get("provincia").setValue(agency.provincia);
            this.agencyForm.get("departamento").setValue(agency.departamento);
            this.agencyForm.get("direccion").setValue(agency.direccion);
            this.agencyForm.get("distrito").setValue(agency.distrito);

          }
        })
  }

  ngOnDestroy(){
    this.observableSubscription.forEach(item => {
      item.unsubscribe();
    });
  }

  save($event:any):void{

    this.formSubmitted = true;
    if(!this.agencyForm.valid){
      return;
    }

    this.saveProduct();

    this.router.navigate(['/agencies']);
  }


  saveProduct():void{
    const agency =new Agency();
    // map data from form to agency
    agency.id = this.agencyForm.get('id').value;
    agency.agencia = this.agencyForm.get('agencia').value;
    agency.provincia = this.agencyForm.get("provincia").value;
    agency.departamento = this.agencyForm.get("departamento").value;
    agency.distrito = this.agencyForm.get("distrito").value;
    agency.direccion = this.agencyForm.get("direccion").value;

    // save to database
    if(agency.id == 0){
      this.agencyService.addNewAgency(agency);}
      else {
        this.agencyService.updateAgency(agency);
      }
  }

}
